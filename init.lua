require('core.packer')
require('core.mappings')
require('plugins.treesitter')
require('plugins.autopairs')
require('plugins.nvimtree')
require('core')
require('ui.statusline')
require('plugins.comment')
require('ui.db')
